from ophyd import PVPositioner,PVPositionerIsClose, EpicsSignal, EpicsSignalRO, Device
from ophyd import Component as Cpt

class PharosDelay(PVPositioner):
    """
    Pharos delay
    """

    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name 

    # this is an initial API 
    setpoint        = Cpt(EpicsSignal,      'DDS120:totalphaseps_set' , kind='config')
    readback        = Cpt(EpicsSignalRO,    'DDS120:totalphaseps',      kind='config')
    done            = Cpt(EpicsSignalRO,    'DDS120:phase_check' ,      kind='config')
    done_value      = 1

    def get(self):
        return self.readback.get()


class PharosAttenuator(PVPositionerIsClose):
    """
    Pharos attenuator
    """

    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name 

    # this is an initial API 
    setpoint        = Cpt(EpicsSignal,      'LASER:TargetAttenuatorPercentageSet' , kind='config')
    readback        = Cpt(EpicsSignalRO,    'LASER:ActualAttenuatorPercentage',     kind='config')

    def get(self):
        return self.readback.get()

class Pharos(Device):
    delay = Cpt(PharosDelay, '', kind='config')
    attenuator = Cpt(PharosAttenuator, '',atol=0.1, kind='config')


