from bluesky import RunEngine
from os.path import expanduser
from tiled.client import from_uri
from IPython import get_ipython
from bluesky.callbacks.zmq import Publisher

# Check if we are in ipython shell
is_ipython = get_ipython()

RE = RunEngine({})

from bessyii.callbacks.best_effort import BestEffortCallback
bec = BestEffortCallback()

# Send all metadata/data captured to the BestEffortCallback.
RE.subscribe(bec)

# create an object db to allow user to interact with the database,
# however, we don't write to it, that is left to the tiled_writer_callback
from os import environ
if environ.get('TILED_URL') is not None and environ.get('TILED_API_KEY') is not None:
    if "http" in environ.get('TILED_URL'):
        db = from_uri(environ.get('TILED_URL'), api_key=environ.get('TILED_API_KEY'))


# Check if ZMQ_URL exists and subscribe to zmq services
if environ.get('ZMQ_URL') is not None:
    #define ip and port for the publisher
    _host = environ.get('ZMQ_URL')

    #create publisher to broadcast documents via 0mq
    publisher = Publisher(_host) 

    #subscribe the publisher to the document stream
    RE.subscribe(publisher)


# Configure persistence between sessions of metadata
# change beamline_name to the name of the beamline
from bluesky.utils import PersistentDict
import os
cwd = os.getcwd()
RE.md = PersistentDict(expanduser('/opt/bluesky/data/persistence/beamline/'))
