# in this file all the imports useful to the beamline
from bessyii.load_script import simple_load

from .beamline import *
from bessyii.preprocessors.avoid_injection import SupplemetalDataInjection
from .base import RE
from bessyii.magics.standard_magics_utils import execute_magic

if 'next_injection' in devices_dictionary.keys():
    from .beamline import next_injection
    repeat_trigger_injection = SupplemetalDataInjection(last_inj=next_injection.time_last_injection, protection_window=2)

    def wait_for_injection(delay=None):
        if repeat_trigger_injection in RE.preprocessors and (delay == None or delay==0):
            RE.preprocessors.remove(repeat_trigger_injection)
            print (f'Wait for injection is deactivated') 
        else:
            if delay == None:
                delay = 2
            if type(delay) == int or type(delay) == float:  
                repeat_trigger_injection.protection_window=delay    
                RE.preprocessors.append(repeat_trigger_injection)
                print (f'Wait for injection is active with a delay of {delay} seconds') 
            else:
                raise ValueError(f'The delay can be only a float or an int. You passed {delay} of type {type(delay)}')

    print('wait_for_injection is available')

else:
    print('wait_for_injection is NOT available')

SL = simple_load(user_script_location='/opt/bluesky/user_scripts/')
load_user_script = SL.load_script
run_plan = execute_magic
